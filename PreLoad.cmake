message(STATUS "in preload")

if (BUILD_FIRMWARE)
  message(STATUS "using fw toolchain")
  set(CMAKE_TOOLCHAIN_FILE cmake/toolchains/fw.cmake CACHE STRING "set toolchain file" )
endif()
