set(CMAKE_SYSTEM_NAME Firmware)
set(CMAKE_CROSSCOMPILING TRUE)

set(CMAKE_C_COMPILER /usr/bin/cc)

add_definitions(-DFIRMWARE)
