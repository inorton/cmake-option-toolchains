# Example use of cmake options and PreLoad.cmake

Say you have a cmake project that builds programs and cross-compiled software
from the same source.

You could do this by using

```
cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/toolchains/linux-arm-cross.cmake ..
```

But that is a lot to type.

Instead by making use of `PreLoad.cmake` we can wrap this up
nice in cmake so that we can now type:

```
cmake -DBUILD_FIRMWARE=1 ..
```
or for normal host builds
```
cmake ..
```
